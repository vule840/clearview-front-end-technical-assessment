import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";
import Members from "./components/Members";
import { Provider } from "react-redux";
import store from "./store";
import Comments from "./components/Comments";

describe("2 tests to be completed", () => {
  test("test if members are rendered", async () => {
    Object.defineProperty(window, "matchMedia", {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
    render(
      <Provider store={store}>
        <Members />
      </Provider>
    );
    const linkElement = await screen.findByText("Praveen");
    expect(linkElement).toBeInTheDocument();
  });

  test("test if comments are rendered", async () => {
    Object.defineProperty(window, "matchMedia", {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    render(
      <Provider store={store}>
        <Comments />
      </Provider>
    );
    const linkElement = await screen.findAllByText("Han Solo");
    expect(linkElement).not.toHaveLength(0);
  });
});
