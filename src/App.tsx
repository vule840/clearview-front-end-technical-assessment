import React, { FC } from "react";

import "antd/dist/antd.css";
import "./App.css";
import { Switch, Route, Link } from "react-router-dom";
import Home from "./components/Home";
import Members from "./components/Members";
import Comments from "./components/Comments";
import { Menu } from "antd";
import {
  HomeOutlined,
  UserAddOutlined,
  CommentOutlined,
  UsergroupAddOutlined,
} from "@ant-design/icons";
import { Row, Col } from "antd";
import Login from "./components/Login";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { authActions } from "./store/authSlice";
import { useHistory } from "react-router-dom";

const App: FC = () => {
  const history = useHistory();
  const auth = useSelector((state: RootStateOrAny) => state.auth.auth);
  const dispatch = useDispatch();
  const logout = () => {
    dispatch(authActions.logout());
    history.push("/login");
  };
  return (
    <div className="App">
      <Menu mode="horizontal">
        <Menu.Item key="mail" icon={<HomeOutlined />}>
          <Link to="/">Home</Link>
        </Menu.Item>
        <Menu.Item key="comment" icon={<CommentOutlined />}>
          <Link to="/comments">Comments</Link>
        </Menu.Item>
        <Menu.Item key="member" icon={<UsergroupAddOutlined />}>
          <Link to="/members">Members</Link>
        </Menu.Item>
        <Menu.Item key="login" icon={<UserAddOutlined />}>
          {!auth ? (
            <Link to="/login">Login</Link>
          ) : (
            <span onClick={logout}>Logout</span>
          )}
        </Menu.Item>
      </Menu>
      <Row className="row">
        <Col xs={{ span: 24, offset: 0 }} lg={{ span: 12, offset: 6 }}>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/comments">
              <Comments />
            </Route>
            <Route path="/members">
              <Members />
            </Route>
            <Route path="/login">{!auth ? <Login /> : "Logout"}</Route>
          </Switch>
        </Col>
      </Row>
    </div>
  );
};

export default App;
