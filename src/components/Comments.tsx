import React, { FC, useState } from "react";
import { Comment, Avatar, Form, Input } from "antd";
import { Radio, Button } from "antd";
import { RootStateOrAny, useSelector, useDispatch } from "react-redux";
import { getComments, addComment } from "../store/forumSlice";
const { TextArea } = Input;

const Comments: FC = () => {
  const comments = useSelector((state: RootStateOrAny) => state.forum.comments);
  const [rerender, setRender] = useState(false);
  const auth = useSelector((state: RootStateOrAny) => state.auth.auth);
  const user = useSelector((state: RootStateOrAny) => state.auth.user);

  const dispatch = useDispatch();
  const [value, setValue] = React.useState("all");

  React.useEffect(() => {
    setRender(false);
    dispatch(getComments());
  }, [rerender, dispatch]);

  const submitHandler = (e) => {
    e.preventDefault();
    const newComment = {
      comment: e.currentTarget.elements[0].value,
      org: user.corp,
    };
    dispatch(addComment(newComment));
    setRender(true);
  };

  const deleteCommentsHandler = () => {
    fetch(`http://localhost:1337/orgs/${user.corp}/comments`, {
      method: "DELETE",
    }).then((res) => res.json());
    setRender(true);
  };

  const onChange = (e) => {
    setValue(e.target.value);
  };
  return (
    <div>
      <h2>Comments</h2>
      {auth ? (
        <Radio.Group onChange={onChange} value={value}>
          <Radio value={"all"}>All comments</Radio>
          <Radio value={"fsociety"}>fsociety</Radio>
          <Radio value={"ecorp"}>ecorp</Radio>
        </Radio.Group>
      ) : (
        ""
      )}

      {comments
        ? comments
            .filter((z) => (value === "all" ? z : z.org === value))
            .map((x) => {
              return (
                <Comment
                  key={x._id}
                  author={"Han Solo"}
                  datetime={x.createdAt}
                  avatar={
                    <Avatar
                      src="https://joeschmoe.io/api/v1/random"
                      alt="Han Solo"
                    />
                  }
                  content={
                    <>
                      <strong>{x.org}</strong>
                      <p>{x.comment}</p>
                    </>
                  }
                />
              );
            })
        : "Pending"}
      {auth ? (
        <>
          {" "}
          <form onSubmit={submitHandler}>
            <Form.Item>
              <TextArea rows={4} />
            </Form.Item>
            <Form.Item>
              <Button htmlType="submit" type="primary">
                Add Comment
              </Button>
            </Form.Item>
          </form>
          <Button onClick={deleteCommentsHandler} type="primary">
            Delete comments
          </Button>
        </>
      ) : (
        <p>"Login to add/delete comments"</p>
      )}
    </div>
  );
};

export default Comments;
