import React from "react";
import { Form, Input, Button, Checkbox, Radio } from "antd";
import { useDispatch } from "react-redux";
import { authActions } from "../store/authSlice";
import { useHistory } from "react-router-dom";

interface ILogin {
  corp: string;
  password: string;
  remember: boolean;
  username: string;
}
const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const onFinish = (values: ILogin) => {
    dispatch(authActions.addUser(values));
    history.push("/members");
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div>
      <Form
        name="basic"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item name="corp" label="Corp">
          <Radio.Group>
            <Radio value="fsociety">Fsociety</Radio>
            <Radio value="ecorp">Ecorp</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{ offset: 8, span: 16 }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;
