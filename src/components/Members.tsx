import { List, Avatar, Space, Radio } from "antd";
import { LikeOutlined, StarOutlined } from "@ant-design/icons";
import { RootStateOrAny, useSelector, useDispatch } from "react-redux";
import React, { FC } from "react";
import { getMembers } from "../store/forumSlice";

interface IMember {
  avatar: string;
  followers: number;
  following: number;
  _id: number;
  email: string;
  org: string;
  createdAt: string;
  updatedAt: string;
  __v: 0;
}

const Members: FC = () => {
  const [value, setValue] = React.useState("all");
  const membersState = useSelector(
    (state: RootStateOrAny) => state.forum.members
  );
  const auth = useSelector((state: RootStateOrAny) => state.auth.auth);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getMembers());
  }, [dispatch]);

  const transformEmailToName = (email) => {
    return email.charAt(0).toUpperCase() + email.slice(1).split("@")[0];
  };

  const IconText = ({ icon, text }) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  );

  const onChange = (e) => {
    setValue(e.target.value);
  };
  return (
    <div>
      <h2>Members</h2>

      {auth ? (
        <Radio.Group onChange={onChange} value={value}>
          <Radio value={"all"}>All members</Radio>
          <Radio value={"fsociety"}>fsociety</Radio>
          <Radio value={"ecorp"}>ecorp</Radio>
        </Radio.Group>
      ) : (
        ""
      )}
      {membersState ? (
        <List
          itemLayout="horizontal"
          dataSource={membersState.filter((z) =>
            value === "all" ? z : z.org === value
          )}
          renderItem={(item: IMember) => (
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar src={item.avatar} />}
                title={transformEmailToName(item.email)}
                description={
                  <div>
                    <p>{item.email}</p>
                    <strong>{item.org}</strong>
                  </div>
                }
                children={item.org}
              />

              <div className="icons">
                <IconText
                  icon={StarOutlined}
                  text={item.followers}
                  key="list-vertical-star-o"
                />

                <IconText
                  icon={LikeOutlined}
                  text={item.following}
                  key="list-vertical-like-o"
                />
              </div>
            </List.Item>
          )}
        />
      ) : (
        "Pending"
      )}
    </div>
  );
};

export default Members;
