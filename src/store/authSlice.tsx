import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
  name: "auth",
  initialState: { auth: false, user: {} },
  reducers: {
    addUser: (state, action) => {
      state.user = action.payload;
      state.auth = !state.auth;
    },
    logout: (state) => {
      state.auth = !state.auth;
    },
  },
});
export const authActions = authSlice.actions;

export default authSlice;
