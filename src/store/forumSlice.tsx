import { createSlice } from "@reduxjs/toolkit";

const forumSlice = createSlice({
  name: "forum",
  initialState: { comments: [], members: [] },
  reducers: {
    addMembers: (state, action) => {
      state.members = action.payload;
    },
    addComments: (state, action) => {
      state.comments = action.payload;
    },
    addComment: (state, action) => {
      state.comments = action.payload;
    },
  },
});

export const getMembers = () => {
  return async (dispatch) => {
    const fetchMembersfsociety = async () => {
      const resp = await fetch("http://localhost:1337/orgs/fsociety/members");
      if (!resp.ok) {
        throw new Error("Something went wrong");
      }
      const data = await resp.json();
      return data;
    };
    const fetchMembersecorp = async () => {
      const resp = await fetch("http://localhost:1337/orgs/ecorp/members");
      if (!resp.ok) {
        throw new Error("Something went wrong");
      }
      const data = await resp.json();
      return data;
    };
    try {
      const fsociety = await fetchMembersfsociety();
      const ecorp = await fetchMembersecorp();
      const allResults = await Promise.all([fsociety, ecorp]);
      dispatch(forumActions.addMembers([...allResults[0], ...allResults[1]]));
    } catch (error) {
      console.log("Some error");
    }
  };
};
export const getComments = () => {
  return async (dispatch) => {
    const fetchCommentsFsociety = async () => {
      const resp = await fetch("http://localhost:1337/orgs/fsociety/comments");
      if (!resp.ok) {
        throw new Error("Something went wrong");
      }
      const data = await resp.json();
      return data;
    };
    const fetchCommentsEcorp = async () => {
      const resp = await fetch("http://localhost:1337/orgs/ecorp/comments");
      if (!resp.ok) {
        throw new Error("Something went wrong");
      }
      const data = await resp.json();
      return data;
    };
    try {
      const fsociety = await fetchCommentsFsociety();
      const ecorp = await fetchCommentsEcorp();
      const allResults = await Promise.all([fsociety, ecorp]);
      dispatch(forumActions.addComments([...allResults[0], ...allResults[1]]));
    } catch (error) {
      //console.log("Some error");
    }
  };
};
export const addComment = (comment) => {
  return async (dispatch) => {
    const addCommentByCorp = async () => {
      const resp = await fetch(
        `http://localhost:1337/orgs/${comment.org}/comments`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(comment),
        }
      );
      if (!resp.ok) {
        throw new Error("Something went wrong");
      }
      const data = await resp.json();
      return data;
    };

    try {
      const corpComment = await addCommentByCorp();
      dispatch(forumActions.addComment(corpComment));
    } catch (error) {
      console.log("Some error");
    }
  };
};

export const forumActions = forumSlice.actions;

export default forumSlice;
