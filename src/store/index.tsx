import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./authSlice";
import forumSlice from "./forumSlice";

const store = configureStore({
  reducer: { forum: forumSlice.reducer, auth: authSlice.reducer },
});

export default store;
